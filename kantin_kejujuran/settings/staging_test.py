from .common import *
import os

DEBUG = True
# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = os.environ.get('SECRET_KEY')

ALLOWED_HOSTS = ['localhost','kantin-kejujuran-staging.herokuapp.com']

# Database
# https://docs.djangoproject.com/en/4.0/ref/settings/#databases
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'd13kvq27qgi7cf',
        'USER': 'llfsebdtpnrpnp',
        'PASSWORD': '72da717b872ce4b8c89047d5331bb2b07f81bb71d2f45063a8609705b35a6d2d',
        'HOST': 'ec2-3-222-74-92.compute-1.amazonaws.com',
        'PORT': '5432',
        'TEST': {
            'NAME': 'd13kvq27qgi7cf', #This is an important entry
        }
    }
}

