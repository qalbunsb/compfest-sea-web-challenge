
from .common import *
import cloudinary
import cloudinary_storage
import os


INSTALLED_APPS += ['cloudinary','cloudinary_storage']

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/4.0/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = os.environ.get('SECRET_KEY')

ALLOWED_HOSTS = ['localhost','kantin-kejujuran-staging.herokuapp.com']

# Database
# https://docs.djangoproject.com/en/4.0/ref/settings/#databases
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'd7f3sl9bc4su55',
        'USER': 'towcfuktxmdniy',
        'PASSWORD': 'b0990004ffe4e4c1a5fbde85b66c96a284f9cb1e20d1502df59697e53a726a0f',
        'HOST': 'ec2-54-160-109-68.compute-1.amazonaws.com',
        'PORT': '5432',
    }
}

CLOUDINARY_STORAGE = {
    'CLOUD_NAME': os.environ.get("CLOUD_NAME"),
    'API_KEY': os.environ.get('API_KEY'),
    'API_SECRET': os.environ.get('API_SECRET'),
}

DEFAULT_FILE_STORAGE = 'cloudinary_storage.storage.MediaCloudinaryStorage'