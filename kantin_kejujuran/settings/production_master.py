
from .common import *
import cloudinary
import cloudinary_storage
import os


INSTALLED_APPS += ['cloudinary','cloudinary_storage']

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/4.0/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = os.environ.get('SECRET_KEY')

ALLOWED_HOSTS = ['localhost', 'kantin-kejujuran-compfest.herokuapp.com']

# Database
# https://docs.djangoproject.com/en/4.0/ref/settings/#databases
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'dchnkqu5q1us01',
        'USER': 'ajgrslbfcfuoqg',
        'PASSWORD': '977815bda78693e01aa20760349db1056fb3da2d9932faa8c589000d003c7d3d',
        'HOST': 'ec2-34-233-115-14.compute-1.amazonaws.com',
        'PORT': '5432',
    }
}

CLOUDINARY_STORAGE = {
    'CLOUD_NAME': os.environ.get("CLOUD_NAME"),
    'API_KEY': os.environ.get('API_KEY'),
    'API_SECRET': os.environ.get('API_SECRET'),
}

DEFAULT_FILE_STORAGE = 'cloudinary_storage.storage.MediaCloudinaryStorage'