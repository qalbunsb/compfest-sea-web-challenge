[![pipeline status](https://gitlab.com/qalbunsb/compfest-sea-web-challenge/badges/master/pipeline.svg)](https://gitlab.com/qalbunsb/compfest-sea-web-challenge/-/commits/master)
[![coverage report](https://gitlab.com/qalbunsb/compfest-sea-web-challenge/badges/master/coverage.svg)](https://gitlab.com/qalbunsb/compfest-sea-web-challenge/-/commits/master)

# Kantin Kejujuran
>URL : https://kantin-kejujuran-compfest.herokuapp.com/

Kantin Kejujuran is a canteen website for students of SD SEA Sentosa where students can buy and add items for sale. It also has a balance box where students with valid ID can deposit or withdraw money in the box

## Features
1. Add and buy items from the canteen
2. Deposit and withdraw money from the balance box
3. Authentication features so that only students from SD SEA Sentosa can buy items from the canteen and make changes to the balance box
4. Sort items by item name and item date

## Tech Stacks
1. Python Django (Backend)
2. Bootstrap 4 (Frontend)
3. Javascript and Jquery (Frontend)
4. Heroku (Deployment)
5. PostgreSQL (Databases)

## Best Practices
1. Canteen's Balance Box model as a Singleton object
2. Unit tests and code coverage
3. Asynchronous programming for frontend

## How to install
In case you need to run this project on a local environment, you can do it by following these steps below. These steps are intended for running this project on a Windows OS, therefore some steps may require some adjustments to be executed in another Operating System.
1. Setup python virtual environment.
    ```powershell
    python -m venv env
    ```
2. Activate the virtual environment.
    ```powershell
    .\env\Scripts\activate
    ```
3. Clone this repository on your local machine.
    ```bash
    git clone https://gitlab.com/qalbunsb/compfest-sea-web-challenge.git
    ```
4. Install the required project dependencies on the virtual environment.
    ```powershell
    pip install -r requirements.txt
    ```
5. This project uses a local PostgreSQL database to run in development environment. To do this, make sure you have [PostgreSQL installed on your local machine](https://www.postgresql.org/download/). After that, you need to create database based on the  *kantin_kejujuran/settings/development.py* credentials, or you can reconfigure the settings to match your own database.

6. To create the database based on the default settings, login to PostgreSQL terminal from cmd prompt as a user with postgres username.
    ```powershell
    psql -U postgres
    ```
7. After being logged into psql terminal, create the database by using the following command.
    ```psql
    CREATE DATABASE kantin_kejujuran;
    ```
8. Exit the psql terminal. Go back to the repository terminal and migrate
    ```powershell
    python manage.py migrate
    ```
9. Finally, run the project using the following command
    ```powershell
    python manage.py runserver
    ```
10. The app can be open in the http://localhost:8000/ url. Now you're good to go!





