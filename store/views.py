from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import TemplateView,DetailView
from django.contrib.auth.mixins import LoginRequiredMixin
from rest_framework.generics import CreateAPIView, ListAPIView, RetrieveAPIView, UpdateAPIView, DestroyAPIView
from rest_framework.filters import OrderingFilter
from .models import BalanceBox, Item
from .serializers import BalanceBoxSerializer, DecreaseBalanceBoxAmountSerializer, IncreaseBalanceBoxAmountSerializer, ItemSerializer
from .forms import ItemForm,BalanceBoxForm
from rest_framework.authentication import SessionAuthentication
from rest_framework.permissions import IsAuthenticated

class CreateItemView(CreateAPIView):
    serializer_class = ItemSerializer
    authentication_classes = [SessionAuthentication]
    permission_classes = [IsAuthenticated]

class GetItemView(ListAPIView):
    serializer_class = ItemSerializer
    queryset = Item.objects.all()
    filter_backends = [OrderingFilter]
    ordering_fields = ['name', 'date']
    ordering = ['name']


class GetDetailItemView(RetrieveAPIView):
    serializer_class = ItemSerializer
    queryset = Item.objects.all()
    lookup_url_kwarg = "id"

class DeleteDetailItemView(DestroyAPIView):
    serializer_class = ItemSerializer
    authentication_classes = [SessionAuthentication]
    permission_classes = [IsAuthenticated]
    queryset = Item.objects.all()
    lookup_url_kwarg = "id"

class GetBalanceBoxView(RetrieveAPIView):
    serializer_class = BalanceBoxSerializer
    authentication_classes = [SessionAuthentication]
    permission_classes = [IsAuthenticated]

    def get_object(self):
        obj = BalanceBox.get_solo()
        return obj

class UpdateBalanceBoxView(UpdateAPIView):
    serializer_class = BalanceBoxSerializer
    authentication_classes = [SessionAuthentication]
    permission_classes = [IsAuthenticated]
    def get_object(self):
        obj = BalanceBox.get_solo()
        return obj

class IncreaseBalanceBoxView(UpdateBalanceBoxView):
    serializer_class = IncreaseBalanceBoxAmountSerializer

class DecreaseBalanceBoxView(UpdateBalanceBoxView):
    serializer_class = DecreaseBalanceBoxAmountSerializer
    
class StoreView(TemplateView):
    template_name = 'index.html'
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['itemForm'] = ItemForm()
        return context

class DetailStoreView(DetailView):
    model = Item
    template_name = 'detail_item.html'
    pk_url_kwarg = 'id'

class BalanceBoxView(LoginRequiredMixin, TemplateView):
    login_url = reverse_lazy('accounts:login')
    template_name = 'balance_box.html'
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['increaseBalanceForm'] = BalanceBoxForm()
        context['decreaseBalanceForm'] = BalanceBoxForm()
        return context

