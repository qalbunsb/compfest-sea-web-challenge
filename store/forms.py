from .serializers import BalanceBoxSerializer, IncreaseBalanceBoxAmountSerializer,DecreaseBalanceBoxAmountSerializer
from drf_braces.forms.serializer_form import SerializerForm
from django.forms import ModelForm, forms
from .models import BalanceBox, Item

class BalanceBoxForm(ModelForm):
    def __init__(self,*args,**kwargs):
        super().__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control'
    class Meta:
        model = BalanceBox
        fields = '__all__'

class ItemForm(ModelForm):
    def __init__(self,*args,**kwargs):
        super().__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control'
    class Meta:
        model = Item
        fields = '__all__'

