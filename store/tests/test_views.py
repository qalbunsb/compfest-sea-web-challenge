from django.test import TestCase
from rest_framework.test import APITestCase
from django.urls import reverse
from store.forms import ItemForm,BalanceBoxForm
from store.models import BalanceBox
from store.views import UpdateBalanceBoxView
from rest_framework import status
from django.contrib.auth import get_user_model

class GetBalanceBoxViewTest(APITestCase):
    def setUp(self):
        self.balance_box = BalanceBox.get_solo()
        self.balance_box.balance = 15
        self.balance_box.save() 

        User = get_user_model()
        self.client.force_login(User.objects.get_or_create(student_id='21508')[0])

    def test_get_method_returns_correct_response(self):   
        url = reverse('store:get_balance_box')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data,{'id':1, 'balance' : 15})

class UpdateBalanceBoxViewTest(APITestCase):
    def setUp(self):
        self.balance_box = BalanceBox.get_solo()
        self.balance_box.balance = 15
        self.balance_box.save() 

    def test_get_object_method_returns_correct_instance(self):
        view = UpdateBalanceBoxView()
        balance_box_test = view.get_object()
        self.assertEqual(balance_box_test,self.balance_box)
        self.assertEqual(balance_box_test.balance,self.balance_box.balance)

class TestStoreView(TestCase):
    def test_response_context_is_correct(self):
        response = self.client.get(reverse('store:home'))
        self.assertIsInstance(response.context['itemForm'],ItemForm)

class TestBalanceBoxView(TestCase):
    def setUp(self):
        User = get_user_model()
        self.client.force_login(User.objects.get_or_create(student_id='21508')[0])

    def test_response_context_is_correct(self):
        response = self.client.get(reverse('store:balance_box'))
        self.assertIsInstance(response.context['increaseBalanceForm'],BalanceBoxForm)
        self.assertIsInstance(response.context['decreaseBalanceForm'],BalanceBoxForm)

