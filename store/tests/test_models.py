from django.test import TestCase
from store.models import BalanceBox

class BalanceBoxTestCase(TestCase):
    def test_balance_box_str_method_positive(self):
        balance_box = BalanceBox.get_solo()
        self.assertEqual(balance_box.__str__(),"Balance Box")

    def test_balance_box_str_method_negative(self):
        balance_box = BalanceBox.get_solo()
        self.assertNotEqual(balance_box.__str__(), "Not a Balance Box")
    