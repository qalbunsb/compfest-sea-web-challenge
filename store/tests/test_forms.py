from django.test import TestCase
from store.forms import BalanceBoxForm,ItemForm

class BalanceBoxFormTest(TestCase):
    def setUp(self):
        self.form = BalanceBoxForm()
    
    def test_form_widget_attribute_has_form_control_class(self):
        for field_name,field in self.form.fields.items():
            self.assertEqual(field.widget.attrs['class'],'form-control')
    
class ItemFormTest(TestCase):
    def setUp(self):
        self.form = ItemForm()
    
    def test_form_widget_attribute_has_form_control_class(self):
        for field_name,field in self.form.fields.items():
            self.assertEqual(field.widget.attrs['class'],'form-control')
