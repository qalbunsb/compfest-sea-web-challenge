from django.test import TestCase
from rest_framework.serializers import ValidationError
from store.serializers import IncreaseBalanceBoxAmountSerializer,DecreaseBalanceBoxAmountSerializer
from store.models import BalanceBox

class TestIncreaseBalanceBoxAmountSerializer(TestCase):
    def setUp(self):
        self.serializer_data = {
            'balance': 10,
        }
        self.balance_box = BalanceBox.get_solo()
        self.balance_box.balance = 1
        self.serializer = IncreaseBalanceBoxAmountSerializer(
            instance=self.balance_box,
            data=self.serializer_data
        )
    
    def test_validated_data_returns_correct_amount(self):
        self.serializer.is_valid()
        self.serializer.update(
            instance=self.balance_box,validated_data=self.serializer_data
        )
        self.assertEqual(self.serializer.validated_data['balance'], self.balance_box.balance)
    
    def test_validated_data_should_not_return_correct_amount(self):
        self.serializer.is_valid(raise_exception=True)
        self.serializer.update(
            instance=self.balance_box,validated_data=self.serializer_data
        )
        self.balance_box.balance += 1
        self.assertNotEqual(self.serializer.validated_data['balance'], self.balance_box.balance)
        
class TestDecreaseBalanceBoxAmountSerializer(TestCase):
    def setUp(self):
        self.serializer_data = {
            'balance': 10,
        }


    def test_validated_data_returns_correct_amount(self):
        self.balance_box = BalanceBox.get_solo()
        self.balance_box.balance = 15
        self.serializer = DecreaseBalanceBoxAmountSerializer(
            instance=self.balance_box,
            data=self.serializer_data
        )
        self.serializer.is_valid(raise_exception=True)
        self.serializer.update(
            instance=self.balance_box,validated_data=self.serializer.validated_data
        )
        self.assertEqual(self.serializer.validated_data['balance'], self.balance_box.balance)

    def test_data_greater_than_box_balance_returns_exception(self):
        self.balance_box = BalanceBox.get_solo()
        self.balance_box.balance = 5
        self.serializer = DecreaseBalanceBoxAmountSerializer(
            instance=self.balance_box,
            data=self.serializer_data
        )
        with self.assertRaisesRegex(ValidationError,expected_regex="The withdrawal value is greater than current's box balance"):
            self.serializer.is_valid(raise_exception=True)
