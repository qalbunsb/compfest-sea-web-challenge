from django.urls import path
from . import views

urlpatterns = [
    path('',views.StoreView.as_view(),name="home"),
    path('<int:id>',views.DetailStoreView.as_view(),name="detail_item"),
    path('items',views.GetItemView.as_view(),name='get_items'),
    path('create',views.CreateItemView.as_view(),name='create_item'),
    path('items/<int:id>',views.GetDetailItemView.as_view(),name='detail_item_api'),
    path('items/delete/<int:id>',views.DeleteDetailItemView.as_view(),name='delete_item_api'),
    path('balance',views.BalanceBoxView.as_view(),name='balance_box'),
    path('balance_box',views.GetBalanceBoxView.as_view(),name="get_balance_box"),
    path('balance_box/increment',views.IncreaseBalanceBoxView.as_view(),name="increment_balance_box"),
    path('balance_box/decrement',views.DecreaseBalanceBoxView.as_view(),name="decrement_balance_box"),
]