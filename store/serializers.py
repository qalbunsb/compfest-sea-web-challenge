from rest_framework import serializers
from .models import BalanceBox, Item

class ItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = Item
        fields = '__all__'

class BalanceBoxSerializer(serializers.ModelSerializer):
    class Meta:
        model = BalanceBox
        fields = '__all__'

class IncreaseBalanceBoxAmountSerializer(BalanceBoxSerializer):
    def validate(self,attrs):
        balance_box = BalanceBox.get_solo()
        attrs['balance'] += balance_box.balance 
        return super().validate(attrs)

class DecreaseBalanceBoxAmountSerializer(BalanceBoxSerializer):
    def validate(self, attrs):
        balance_box = self.instance
        if  balance_box.balance - attrs["balance"] < 0:
            raise serializers.ValidationError({"balance": "The withdrawal value is greater than current's box balance"})
        attrs['balance'] = balance_box.balance - attrs['balance']
        return super().validate(attrs)