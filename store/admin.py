from django.contrib import admin
from solo.admin import SingletonModelAdmin
from .models import Item,BalanceBox
# Register your models here.
admin.site.register(Item)
admin.site.register(BalanceBox,SingletonModelAdmin)
