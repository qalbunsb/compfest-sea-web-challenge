from datetime import datetime
from django.db import models
from solo.models import SingletonModel

# Create your models here.
class Item(models.Model):
    name = models.CharField(max_length=30)
    image = models.ImageField(upload_to="images")
    desc = models.CharField(max_length=100)
    price = models.PositiveIntegerField()
    date = models.DateTimeField(auto_now_add=True, blank=True)

class BalanceBox(SingletonModel):
    balance = models.PositiveBigIntegerField(default=0)

    def __str__(self):
        return "Balance Box"

    class Meta:
        verbose_name = "Balance Box"


