from django.contrib.auth.base_user import BaseUserManager
from django.contrib.auth.models import UserManager
from django.contrib.auth.hashers import make_password
from django.apps import apps

class UserManager(BaseUserManager):
    use_in_migrations = True
    
    def _create_user(self, student_id, password, **extra_fields):
        """
        Create and save a user with the given student id and password.
        """
        if not student_id:
            raise ValueError("The given student id must be set")
        
        GlobalUserModel = apps.get_model(
            self.model._meta.app_label, self.model._meta.object_name
        )
        student_id = GlobalUserModel.normalize_username(student_id)
        user = self.model(student_id=student_id, **extra_fields)
        user.password = make_password(password)
        user.save(using=self._db)
        return user
    
    def create_user(self, student_id, password=None, **extra_fields):
        extra_fields.setdefault("is_staff", False)
        extra_fields.setdefault("is_superuser", False)
        return self._create_user(student_id, password, **extra_fields)
    
    def create_superuser(self, student_id,password=None, **extra_fields):
        extra_fields.setdefault("is_staff", True)
        extra_fields.setdefault("is_superuser", True)

        if extra_fields.get("is_staff") is not True:
            raise ValueError("Superuser must have is_staff=True.")
        if extra_fields.get("is_superuser") is not True:
            raise ValueError("Superuser must have is_superuser=True.")

        return self._create_user(student_id,password, **extra_fields)
