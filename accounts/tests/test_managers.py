from multiprocessing.sharedctypes import Value
from django.test import TestCase
from django.contrib.auth import get_user_model
from django.contrib.auth.hashers import make_password

class TestUserManager(TestCase):
    def test_create_user_without_student_id_returns_value_error(self):
        User = get_user_model()
        self.assertRaisesRegex(ValueError,'The given student id must be set',User.objects.create_user,None)

    def test_create_user_with_correct_student_id_returns_user_instance_with_correct_attribute(self):
        User = get_user_model()
        user = User.objects.create_user(student_id='21508')
        self.assertIsInstance(user,User)
        self.assertEqual(user.student_id,'21508')
    
    def test_create_superuser_with_correct_value_returns_user_instance_with_correct_attribute(self):
        User = get_user_model()
        user = User.objects.create_superuser(student_id='21508')
        self.assertIsInstance(user,User)
        self.assertEqual(user.student_id,'21508')

    def test_create_superuser_with_is_staff_value_false_raise_valueerror_exception(self):
        User = get_user_model()
        self.assertRaisesRegex(ValueError,'Superuser must have is_staff=True.',User.objects.create_superuser,student_id='21508',is_staff=False)

    def test_create_superuser_with_is_superuser_value_false_raise_valueerror_exception(self):
        User = get_user_model()
        self.assertRaisesRegex(ValueError,'Superuser must have is_superuser=True.',User.objects.create_superuser,student_id='21508',is_superuser=False)
