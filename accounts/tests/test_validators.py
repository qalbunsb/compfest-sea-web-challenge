from django.test import TestCase
from accounts.validators import validate_student_id
from django.core.exceptions import ValidationError

class TestStudentValidators(TestCase):
    def test_validate_student_id_function_validate_correctly(self):
        try:
            validate_student_id('21508')
        except ValidationError:
            self.fail("validate_student_id should not raise ValidationError on valid input")
    
    def test_validate_student_id_function_returns_validation_error_on_incorrect_input(self):
        self.assertRaisesRegex(ValidationError,'The sum of first three digits is not equal to the two last digits',validate_student_id,'11199')
        self.assertRaisesRegex(ValidationError,'Input must be a 5 digit number',validate_student_id,'abcde')
        self.assertRaisesRegex(ValidationError,'Input must be a 5 digit number',validate_student_id,'123456789')