from unicodedata import name
from django.urls import path
from . import views
from django.contrib.auth.views import LogoutView

urlpatterns = [
    path('register',views.RegistrationView.as_view(),name='register'),
    path('login',views.AccountLoginView.as_view(),name='login'),
    path('logout',views.AccountLogoutView.as_view(),name='logout')
]