from django.db import models
from django.contrib.auth.models import AbstractBaseUser,PermissionsMixin,AbstractUser
from .validators import validate_student_id
from .managers import UserManager

class User(AbstractBaseUser,PermissionsMixin):
    student_id = models.CharField(max_length=5,unique=True,validators=[validate_student_id])
    date_joined = models.DateTimeField(auto_now_add=True)
    is_staff = models.BooleanField(default=False)
    objects = UserManager()

    USERNAME_FIELD = 'student_id'
    

    

        