from django.core.exceptions import ValidationError

def validate_student_id(student_id):
        try:
            if len(student_id) > 5:
                raise ValidationError("Input must be a 5 digit number")
            int(student_id)
            sum_first_three_digits = sum(list(map(lambda x: int(x),student_id[0:3])))
            last_two_digits = int(student_id[3:5])
            if sum_first_three_digits != last_two_digits:
                raise ValidationError("The sum of first three digits is not equal to the two last digits")
        except ValueError as e:
            raise ValidationError("Input must be a 5 digit number")