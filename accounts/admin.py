from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin

from .models import User

class CustomUserAdmin(BaseUserAdmin):
    ordering = ('student_id',)
    list_display = ('student_id', 'date_joined', 'is_staff')
    list_filter = ('date_joined', 'is_staff')

    fieldsets = (
        (None, {'fields': ('student_id', 'password')}),
        ('Permissions', {'fields': ('is_superuser','is_staff',)}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('student_id', 'password1', 'password2'),
        }),
    )

admin.site.register(User, CustomUserAdmin)