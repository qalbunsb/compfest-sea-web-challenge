from django.shortcuts import render
from django.views.generic import CreateView
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib.auth.views import LoginView,LogoutView
from django.urls import reverse_lazy
from .forms import CustomUserCreationForm
# Create your views here.
class RegistrationView(SuccessMessageMixin,CreateView):
    template_name = 'registration.html'
    success_url = reverse_lazy('accounts:login')
    form_class = CustomUserCreationForm
    success_message = "Your profile was created successfully"

class AccountLoginView(LoginView):
    template_name = 'login.html'
    next_page = reverse_lazy('store:home')

class AccountLogoutView(LogoutView):
    next_page = reverse_lazy('accounts:login')

    